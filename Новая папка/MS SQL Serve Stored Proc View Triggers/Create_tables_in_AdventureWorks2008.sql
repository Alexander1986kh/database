use [AdventureWorks2008]
IF EXISTS (SELECT * FROM sys.tables where [name] = 'USERS_TO_ROLES')
DROP TABLE USERS_TO_ROLES;--����� ������������
create table USERS_TO_ROLES(
ID int identity(1,1) not null,
[USER_ID] int not null,
ROLE_ID int not null
);

IF EXISTS (SELECT * FROM sys.tables where [name] = 'USERS')
DROP TABLE USERS;
create table USERS(
ID int identity(1,1) not null,
[USER_NAME] varchar(20) not null,
);

IF EXISTS (SELECT * FROM sys.tables where [name] = 'ROLES')
DROP TABLE ROLES;
create table ROLES(
ID int identity(1,1) not null,
ROLE_NAME varchar(20) not null,--�������� ����, � ����������� � ������� ������������
[RULE] char(20) not null,--(������ U � ������, ������� �������� � ������� RULE � �������� ����� ��������� ������,
-- ������ I � ����� ��������� ������, ������ D � ����� �������) 
);



IF EXISTS (SELECT * FROM sys.tables where [name] = 'MYLOGS')
DROP TABLE MYLOGS;

create table MYLOGS(
MYLOGS_ID int identity(1,1) not null,
[ACTION] nchar(20) not null,--�������� ��� �������� ��� �����
TBL_NAME varchar(20) not null,--���� �������� ��� ��������- �� ��������
ID_ROW nchar(20) not null,--ID ������ �������, ���� ��������� � ���
LOG_INFO varchar(20) not null,--���������� ��������
LOG_TIME date not null--����� �������
);

ALTER TABLE [USERS_TO_ROLES] ADD CONSTRAINT PK_USERS_TO_ROLES_ID PRIMARY KEY (ID);
ALTER TABLE [USERS] ADD CONSTRAINT PK_USERS_ID PRIMARY KEY (ID);
ALTER TABLE [ROLES] ADD CONSTRAINT PK_ROLES_ID PRIMARY KEY (ID);
ALTER TABLE [MYLOGS] ADD CONSTRAINT PK_MYLOGS_MYLOGS_ID PRIMARY KEY (MYLOGS_ID);
ALTER TABLE [USERS_TO_ROLES] ADD CONSTRAINT FK_USERS_TO_ROLES_USER_ID FOREIGN KEY ([USER_ID]) REFERENCES USERS(ID);
ALTER TABLE [USERS_TO_ROLES] ADD CONSTRAINT FK_USERS_TO_ROLES_ROLE_ID FOREIGN KEY (ROLE_ID) REFERENCES ROLES(ID);
IF EXISTS (SELECT * FROM SYS.triggers WHERE [NAME] ='my_safety_create_log_trigger')
--IF (OBJECT_ID (N'my_safety_create_log_trigger',N'TR') is not null)
drop trigger  my_safety_create_log_trigger ON DATABASE;
	GO
 CREATE TRIGGER my_safety_create_log_trigger
 ON DATABASE 
 FOR CREATE_TABLE 
 AS
 PRINT N'��������� �������� CREATE_TABLE';

 go
 IF EXISTS (SELECT * FROM SYS.triggers WHERE [NAME] ='my_safety_drop_log_trigger')
--IF (OBJECT_ID (N'my_safety_create_log_trigger',N'TR') is not null)
drop trigger  my_safety_drop_log_trigger ON DATABASE;
	GO
 CREATE TRIGGER my_safety_drop_log_trigger
 ON DATABASE 
 FOR DROP_TABLE 
 AS
 PRINT N'��������� �������� DROP_TABLE';
 go

  IF EXISTS (SELECT * FROM SYS.triggers WHERE [NAME] ='my_safety_alter_log_trigger')
--IF (OBJECT_ID (N'my_safety_create_log_trigger',N'TR') is not null)
drop trigger  my_safety_alter_log_trigger ON DATABASE;
	GO
 CREATE TRIGGER my_safety_alter_log_trigger
 ON DATABASE 
 FOR ALTER_TABLE 
 AS
 PRINT N'��������� �������� ALTER_TABLE';