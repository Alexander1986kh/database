use AdventureWorks2008
IF EXISTS (SELECT * FROM SYS.triggers WHERE [NAME] ='my_safety_create_log_trigger')
--IF (OBJECT_ID (N'my_safety_create_log_trigger',N'TR') is not null)
drop trigger  my_safety_create_log_trigger ON DATABASE;
	GO
 CREATE TRIGGER my_safety_create_log_trigger
 ON DATABASE 
 FOR CREATE_TABLE 
 AS
 PRINT N'выполнена операция CREATE_TABLE';

 go
 IF EXISTS (SELECT * FROM SYS.triggers WHERE [NAME] ='my_safety_drop_log_trigger')
--IF (OBJECT_ID (N'my_safety_create_log_trigger',N'TR') is not null)
drop trigger  my_safety_drop_log_trigger ON DATABASE;
	GO
 CREATE TRIGGER my_safety_drop_log_trigger
 ON DATABASE 
 FOR DROP_TABLE 
 AS
 PRINT N'выполнена операция DROP_TABLE';
 go

  IF EXISTS (SELECT * FROM SYS.triggers WHERE [NAME] ='my_safety_alter_log_trigger')
--IF (OBJECT_ID (N'my_safety_create_log_trigger',N'TR') is not null)
drop trigger  my_safety_alter_log_trigger ON DATABASE;
	GO
 CREATE TRIGGER my_safety_alter_log_trigger
 ON DATABASE 
 FOR ALTER_TABLE 
 AS
 PRINT N'выполнена операция ALTER_TABLE';