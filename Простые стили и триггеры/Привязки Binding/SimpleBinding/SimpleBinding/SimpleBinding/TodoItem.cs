﻿namespace SimpleBinding
{
    internal class TodoItem
    {
        public string Title { get; internal set; }
        public int Completion { get; internal set; }
    }
}