 use [C:\DEVMED\SAMPLES\WPF\DYNAMICLISTVIEW\DATA\NORTHWIND.MDF]

--1. ������� ��������, � ������� ���� ������ 
select 
customers.ContactName
from
[dbo].[Customers] as customers
left join 
[dbo].[Orders] as orders
on orders.CustomerID=customers.CustomerID
where orders.OrderID is not null;

--2. ������� ���� ��������, � ������� ��� �������� ������
select 
customers.ContactName
from
[dbo].[Customers] as customers
left join 
[dbo].[Orders] as orders
on orders.CustomerID=customers.CustomerID
where orders.OrderID is null;
-- 3. ������� ��������, � ������� ������ ���� ����� 
select 
customers.ContactName,
count(customers.ContactName) as number_orders
from
[dbo].[Customers] as customers
left join 
[dbo].[Orders] as orders
on orders.CustomerID=customers.CustomerID
where orders.OrderID is not null
group by customers.ContactName
Having count(customers.ContactName)=1;
--4. ������� ��������, � ������� ����� ���� ������� 
select 
customers.ContactName,
count(customers.ContactName) as number_orders
from
[dbo].[Customers] as customers
left join 
[dbo].[Orders] as orders
on orders.CustomerID=customers.CustomerID
where orders.OrderID is not null
group by customers.ContactName
Having count(customers.ContactName)>2;
--5. ������� ���� ��������, � ������� �� ����� Fax 
select 
customers.ContactName
from
[dbo].[Customers] as customers
where customers.Fax is null

--6. ������� ���� ��������, � ������� ����� ������
select 
customers.ContactName
from
[dbo].[Customers] as customers
where customers.Region is not null
--7. ������� ���� ��������, � ������� PostalCode ���������� � ���� 
select
customers.ContactName,
customers.PostalCode
from
[dbo].[Customers] as customers
where customers.PostalCode LIKE '0%';
--8. ������� ���� ��������, � ������� PostalCode ������� 5 �������� 
select
customers.ContactName,
customers.PostalCode
from
[dbo].[Customers] as customers
where len(customers.PostalCode)>5;
--9. ������� ���� ��������, � ������� PostalCode ���������� � ���� � ������� 5 �������� 
select
customers.ContactName,
customers.PostalCode
from
[dbo].[Customers] as customers
where len(customers.PostalCode)>5 AND customers.PostalCode LIKE '0%';
--10. ������� ���� ��������, � ������� Phone ������������� �� 4 
select
customers.ContactName,
customers.Phone
from
[dbo].[Customers] as customers
where customers.Phone LIKE '%4';
--11. ������� ���� ��������, � ������� Phone �������� ������ 
select
customers.ContactName,
customers.Phone
from
[dbo].[Customers] as customers
where customers.Phone LIKE '%(%)%';
--12. ������� ���� ��������, � ������� Phone ��� ������� � ������� � ����� ��� 1 ������ 
select
customers.ContactName,
customers.Phone
from
[dbo].[Customers] as customers
where customers.Phone LIKE '%(_)%';
--13. ������� ���� ��������, � ������� Phone ��� ������� � ������� � ����� ��� 3 ������� 
select
customers.ContactName,
customers.Phone
from
[dbo].[Customers] as customers
where customers.Phone LIKE '%(___)%';
--14. ������� ���� ��������, � ������� Phone ����� ������� ��� 

