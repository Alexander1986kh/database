﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//Привязка к свойствам другого элемента разметки
//1. Создать TextBox и TextBlock.Задать для TextBox красный фон.
//a) Создать привязку, которая позволит менять содержимое TextBlock синхронно с изменением
//содержимого в TextBox.
//б) Создать привязку, которая установит для TextBlock фон такого цвета, как шрифт TextBox;
//в) Создать привязку, которая установит для TextBlock такой цвет шрифта, как фонTextBox;

namespace BindingSimpleProject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window,INotifyPropertyChanged
    {
        private int count;
        public int Count
        {
            get => count;
            set
            {
                count = value;
                NotifyPropertyChanged("Count");
            }
        }
        private ObservableCollection<string> list;
        private DataSet dataSet;
        // Коллекция, которая уведомляет интерфейсные компоненту о произошедших в ней изменениях
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;//Реализация интерфейса для отображения изменений свойств класса
        //в интерфейсе
        private void NotifyPropertyChanged(string PropertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
        #endregion
      

        public MainWindow()
        {          
            InitializeComponent();
            list = new ObservableCollection<string>();
            list.Add("user1");
            list.Add("user2");
            list.Add("user3");
            list.Add("user4");
            Count = 0;
            dataSet = new DataSet();
            ElementToElementBinding();
            ElementToMainWindow();
            ClassPropertyBinding();
            ListBoxBindingToCollection();
            getPersonDataFromDB();
            ListBoxToDatasetBinding();
        }
        private void ElementToElementBinding()
        {
            txtblock.DataContext = txtBox;
        }
        private void ElementToMainWindow()
        {
            txtWindowHeight.DataContext = this;
            txtWindowWidth.DataContext = this;
        }
        private void ClassPropertyBinding()
        {
            txtBlockCount.DataContext = this;
        }
        private void btnClick_Click(object sender, RoutedEventArgs e)
        {
            Count++;    
        }
        private void ListBoxBindingToCollection()
        {
            LBoxUsers.ItemsSource = this.list;
        }
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            list.Add("newUser");
        }
        private void getPersonDataFromDB()
        {
            try
            {
                string ConnectionString = ConfigurationManager.ConnectionStrings["AdventureWorksConnection"].ConnectionString;
                SqlConnection connection = new SqlConnection(ConnectionString);
                connection.Open();
                SqlDataAdapter sqldataAdapter = new SqlDataAdapter("SELECT TOP 10 * FROM[Person].[Person]", connection);
                sqldataAdapter.Fill(dataSet, "Persons");
            }
            catch(SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void ListBoxToDatasetBinding()
        {
            LBoxPerson.ItemsSource = dataSet.Tables["Persons"].AsDataView();
        }
    }
}
