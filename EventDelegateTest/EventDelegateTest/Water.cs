﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventDelegateTest
{
    public class Water
    {
        public string type { get; set; }
        public Water(string aType)
        {
            type = aType;
        }
        public void run()
        {
            if (isRunning != null)
            {
                isRunning(String.Format("{0} water is running", type));
            }
        }
        public void stop()
        {
            if (isStopped != null)
            {
                isStopped(String.Format("{0} water was stopped", type));
            }
        }
        public  delegate void WaterCondition(string message);
        public event WaterCondition isRunning;
        public event WaterCondition isStopped;
    }
}
