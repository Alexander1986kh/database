use master 

IF EXISTS(SELECT 'True' FROM sys.databases WHERE name LIKE 'db28pr8') DROP DATABASE db28pr8
GO
CREATE DATABASE db28pr8
GO
USE [db28pr8]
CREATE TABLE [dbo].[CUSTOMERS](
	[ID] [int] IDENTITY(1,1) primary key NOT NULL,
	[NICK_NAME] [nvarchar](50) NULL,
	[PHONE] [nchar](20) NOT NULL,
	[EMAIL] [nvarchar](50) NOT NULL,
	[ID_MEN] [int] NULL
)
GO
CREATE TABLE [dbo].[POSITIONS](
	[ID] [int] IDENTITY(1,1) primary key NOT NULL,
	[NAME] [nvarchar](50) NOT NULL,
	[ROLE] [nvarchar](50) NULL
)
GO
CREATE TABLE [dbo].[MEN](
	[ID] [int] IDENTITY(1,1) primary key NOT NULL,
	[NAME] [nvarchar](50) NOT NULL,
	[LNAME] [nvarchar](50) NOT NULL,
	[YEAR] [int] NOT NULL
)
GO
CREATE TABLE [dbo].[EMPLOYEES](
	[ID] [int] IDENTITY(1,1) primary key NOT NULL,
	[CODE] [nvarchar](50) NOT NULL,
	[ID_MEN] [int] NOT NULL,
	[ID_POSITIONS] [int] NOT NULL
)
GO
CREATE TABLE [dbo].[MANAGER](
	[ID] [int] IDENTITY(1,1) primary key NOT NULL,
	[NAME] [nvarchar](50) NOT NULL,
	[ID_EMPLOYEES] [int] NULL,
	[ID_POSITIONS] [int] NULL
)
GO
CREATE TABLE [dbo].[DEPARTMENTS](
	[ID] [int] IDENTITY(1,1) primary key NOT NULL,
	[NAME] [nvarchar](50) NOT NULL,
	[ID_MANAGER] [int] NULL,
)
GO
CREATE TABLE [dbo].[CLIENTS](
	[CLIENTS_ID] [int] IDENTITY(1,1) primary key NOT NULL,
	[CLIENTS_NAME] [nvarchar](50) NOT NULL,
	[CLIENTS_PHONE] [nchar](20) NOT NULL,
	[CLIENTS_MAIL] [nvarchar](50) NOT NULL,
)
GO
CREATE TABLE [dbo].[PRODUCTS](
	[PRODUCTS_ID] [int] IDENTITY(1,1) primary key NOT NULL,
	[PRODUCTS_NAME] [nvarchar](50) NULL,
	[PRICE] [float] NOT NULL
)
GO
CREATE TABLE [dbo].[BANKS](
	[BANKS_ID] [int] IDENTITY(1,1) primary key NOT NULL,
	[BANKS_NAME] [nvarchar](50) NOT NULL,
	[REGION_INFO] [nvarchar](50) NOT NULL
)
GO
CREATE TABLE [dbo].[ACCOUNTS](
	[ACCOUNTS_ID] [int] IDENTITY(1,1) primary key NOT NULL,
	[DESCRIPTION] [nvarchar](50) NOT NULL,
	[BANKS_ID] [int] NOT NULL,
	[ACCOUNTS_SUM] [int] NOT NULL,
	[ACCOUNT] [int] NOT NULL
)
GO
CREATE TABLE [dbo].[ACCOUNTS_TO_CLIENTS](
	[ACCOUNTS_TO_CLIENTS_ID] [int] IDENTITY(1,1) primary key NOT NULL,
	[ACCOUNTS_ID] [int] NOT NULL,
	[CLIENTS_ID] [int] NOT NULL,
	[INDICATION] [int] NOT NULL
)
GO
CREATE TABLE [dbo].[ORDERS](
	[ORDERS_ID] [int] IDENTITY(1,1) primary key NOT NULL,
	[DESCRIPTION] [nvarchar](50) NOT NULL,
	[ORDERS_DATE] [date] NOT NULL,
	[TOTAL_COSTS] [float] NOT NULL,
	[CLIENTS_ID] [int] NOT NULL
)
GO
CREATE TABLE [dbo].[ORDERS_POSITIONS](
	[ORDERS_POSITIONS_ID] [int] IDENTITY(1,1) primary key NOT NULL,
	[ORDERS_ID] [int] NOT NULL,
	[PRODUCTS_ID] [int] NOT NULL,
	[PRICE] [float] NOT NULL,
	[ITEM_COUNT] [int] NOT NULL
)
GO