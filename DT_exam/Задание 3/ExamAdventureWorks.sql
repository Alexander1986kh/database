use [AdventureWorks2008R2]
--1. ������ ����������, �� ������� ���������� ����� �������� ��� � ����� � ������ ����������, �� ������� � ������

select 
Employee.JobTitle,
EPayH.PayFrequency
from
[HumanResources].[Employee] as Employee
left join  [HumanResources].[EmployeePayHistory] as EPayH
on Employee.BusinessEntityID=EPayH.BusinessEntityID
group by Employee.JobTitle,
EPayH.PayFrequency
order by EPayH.PayFrequency;


-- 2. ������ �������, � ������� �������� ����������, �� ����������, ������������ ��� � ����� 
select 
Dep.Name,
EPH.PayFrequency
from [HumanResources].[Employee] as Employee 
Left join 
[HumanResources].[EmployeePayHistory] as EPH
on Employee.BusinessEntityID=EPH.BusinessEntityID
Left join
[HumanResources].[EmployeeDepartmentHistory] as EDH
on Employee.BusinessEntityID=EDH.BusinessEntityID
Left join
[HumanResources].[Department] as Dep
on EDH.DepartmentID=Dep.DepartmentID
where EPH.PayFrequency=1
group by 
EPH.PayFrequency,
Dep.Name
--3. �������� ������ ����������� � ��������� �� �����, ���������, ���������� ����� � ������ �� ������� 
Select
Person.LastName,
Person.FirstName,
Employee.JobTitle,
EPH.PayFrequency,
EPH.Rate
from
[HumanResources].[Employee] as Employee
left join
[HumanResources].[EmployeePayHistory] as EPH
on Employee.BusinessEntityID=EPH.BusinessEntityID
left join 
[Person].[Person] as Person
on Employee.BusinessEntityID=Person.BusinessEntityID 
order by Person.LastName, Person.FirstName
 desc