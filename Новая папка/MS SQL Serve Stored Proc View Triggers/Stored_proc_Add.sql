use [AdventureWorks2008]
go
create procedure ADD_ROLE
(
@ROLE_NAME VARCHAR(20),
@RULE CHAR(20),
@ID INT out --�������� �������
)
as
begin
 INSERT INTO [dbo].[ROLES] ([ROLE_NAME], [RULE]) VALUES 
 (@ROLE_NAME, @RULE);
 SELECT 
 @ID=SCOPE_IDENTITY();--������ � ���� IDENTITY ������, ������� � ������
end
 
DECLARE @OUT_ID INT;
EXECUTE ADD_ROLE 'ADMIN', 'D',@OUT_ID OUTPUT
SELECT * FROM ROLES
PRINT @OUT_ID;