﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Configuration;



namespace SimpleBindingWpfMsSql
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Department> Departments = new List<Department>();
        private List<Employee> Employees = new List<Employee>();
        private Dictionary<Department, IEnumerable<Employee>> GroupEmployee;
        private DataSet data;
        public MainWindow()
        {
            InitializeComponent();
            GetDataFromDataBase();
            //testTextBlockFilling();
            //testListBoxFilling();
            //testComboBoxFilling();
        }

        private void testComboBoxFilling()
        {
            this.myComboBox.ItemsSource = this.data.Tables["Departments"].AsDataView();

            DataView dv = (DataView)this.myComboBox.ItemsSource;
            dv.Sort = "Departments_ID";
            DataRow[] rows = this.data.Tables["Departments"].Select("Departments_ID=2");
            DataRowView[] rowsView = dv.FindRows(rows[0]["Departments_ID"]);
            this.myComboBox.SelectedItem = rowsView[0];

            //this.myComboBox.SelectedIndex = 1;
        }

        private void testListBoxFilling()
        {
            List<TodoItem> items = new List<TodoItem>();
            items.Add(new TodoItem() { Title = "Complete this WPF tutorial", Completion = 45 });
            items.Add(new TodoItem() { Title = "Learn C#", Completion = 80 });
            items.Add(new TodoItem() { Title = "Wash the car", Completion = 0 });

            //lbTodoList.ItemsSource = items;


            lvTodoList1.ItemsSource = this.data.Tables["Departments"].AsDataView();


            lvTodoList2.ItemsSource = this.data.Tables["Departments"].AsDataView();
            lvTodoList2.SelectedValuePath = "Departments_ID";//Здесь привязка:
                                                            //поэтому речь идет о свойствах класса, они регистрочувствительны
                                                           //Имя свойства должно полностью совпадать буква в букву с базой данных
            lvTodoList2.DisplayMemberPath = "DEPARTMENTS_NAME";

            lvTodoList3.ItemsSource = this.data.Tables["Departments"].AsDataView();
            // Ошибка в коде , для этого компонента ошибка 

            lvTodoList4.ItemsSource = this.data.Tables["Departments"].AsDataView();


        }
        public static DataSet GetCustomers()
        {
            // query
            string sql = @"
                SELECT *
                FROM Customers
             ";

            // create connection
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MSSQL"].ConnectionString);

            try
            {
                // Create data adapter
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = new SqlCommand(sql, conn);

                // create and fill dataset
                DataSet ds = new DataSet();
                da.Fill(ds, "Customers");

                return ds;
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error: " + e);
            }
            finally
            {
                // close connection
                conn.Close();
            }

            return null;
        }
        private void testTextBlockFilling()
        {
            List<TextViewer> list = new List<TextViewer>();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 100; i++)
            {
                list.Add(new TextViewer(String.Format("{0},{1}", i.ToString(), "Some text...")));
                sb.AppendLine(list[i].DisplayText);
            }

            TextViewer tv = new TextViewer(sb.ToString());
            this.txtMyTextBox.DataContext = tv;
        }

        private void GetDataFromDataBase()
        {
            try
            {
                // Specify a connection string. Replace the given value with a 
                // valid connection string for a Northwind SQL Server sample
                // database accessible to your system.
                var connectionString = ConfigurationManager.ConnectionStrings["MSSQL"].ConnectionString;
                SqlConnection connection =
                    new SqlConnection(connectionString);
                connection.Open();

                // Create a DataSet.
                this.data = new DataSet();
                data.Locale = System.Globalization.CultureInfo.InvariantCulture;

                // Add data from the Customers table to the DataSet.
                SqlDataAdapter masterDataAdapter = new
                    SqlDataAdapter("select * from Departments", connection);
                masterDataAdapter.Fill(data, "Departments");

                // Add data from the Orders table to the DataSet.
                SqlDataAdapter detailsDataAdapter = new
                    SqlDataAdapter("select * from Employees", connection);
                detailsDataAdapter.Fill(data, "Employees");

                // Establish a relationship between the two tables.
                DataRelation relation = new DataRelation("EmployeesDepartments",
                    data.Tables["Departments"].Columns["Departments_ID"],
                    data.Tables["Employees"].Columns["Departments_ID"]);
                data.Relations.Add(relation);

                // Bind the master data connector to the Customers table.
                //masterBindingSource.DataSource = data;
                //masterBindingSource.DataMember = "Departments";

                //DataView dv = data.Tables["Departments"].DefaultView;
                //dv.RowFilter = "DepartmentsLocation = 'Kharkiv'";
                //dv.Sort = "DepartmentsLocation";

                //dv = new DataView(data.Tables["Departments"], "DepartmentsLocation = 'London'", "DepartmentsLocation", DataViewRowState.CurrentRows);
                //Create new table based on filtered records
                // DataTable newTable = dv.ToTable("BrazilianContactNames", true, new string[] { "ContactName" });
                /*
                masterBindingSource.DataSource = dv;
                // Bind the details data connector to the master data connector,
                // using the DataRelation name to filter the information in the 
                // details table based on the current row in the master table. 
                detailsBindingSource.DataSource = masterBindingSource;
                detailsBindingSource.DataMember = "EmployeesDepartments";
                */

            }
            catch (SqlException ex)
            {
                MessageBox.Show("To run this example, replace the value of the " +
                    "connectionString variable with a connection string that is " +
                    "valid for your system. : " + ex.ToString());
            }
        }
        public class TodoItem
        {
            public string Title { get; set; }

            public int Completion { get; set; }
        }
        public class TextViewer
        {
            private String datasource;
            public TextViewer(String source)
            {
                this.datasource = source;
            }

            public string DisplayText
            {
                get
                {
                    if (datasource.Contains("<bold>") == false)
                    {
                        //return "<bold>" + datasource + "</bold>";
                        return datasource;
                    }
                    return datasource;
                }
                set { datasource = value; }
            }
        }

        public class Department
        {
            private int deptNo = 0;
            private String deptName = String.Empty;
            private String deptLoc = String.Empty;

            public Department() { }

            public Department(int DeptNo, String DeptName, String DeptLoc)
            {
                deptNo = DeptNo;
                deptName = DeptName;
                deptLoc = DeptLoc;
            }

            public int DeptNo
            {
                set { deptNo = value; }
                get { return deptNo; }
            }

            public String DeptName
            {
                set { deptName = value; }
                get { return deptName; }
            }

            public String DeptLoc
            {
                set { deptLoc = value; }
                get { return deptLoc; }
            }


        }

        public class Employee
        {
            private int employeeNo;
            private String employeeName;
            private int deptNo;
            private String job;
            private Double salary;

            public Employee() { }

            public Employee(int EmployeeNo, String EmployeeName, int DeptNo, String Job, Double Salary)
            {
                employeeNo = EmployeeNo;
                employeeName = EmployeeName;
                deptNo = DeptNo;
                job = Job;
                salary = Salary;
            }
            public int EmployeeNo
            {
                set { employeeNo = value; }
                get { return employeeNo; }
            }
            public String EmployeeName
            {
                set { employeeName = value; }
                get { return employeeName; }
            }
            public int DeptNo
            {
                set { deptNo = value; }
                get { return deptNo; }
            }

            public String Job
            {
                set { job = value; }
                get { return job; }
            }

            public Double Salary
            {
                set { salary = value; }
                get { return salary; }
            }


        }
    }
}
