use [Northwind]
begin transaction
SET IDENTITY_INSERT [dbo].[Employees] ON;

insert into [dbo].[Employees] (EmployeeID,LastName,FirstName) values (0,N'test',N'test');

SET IDENTITY_INSERT [dbo].[Employees] OFF;
commit