   use master
      IF EXISTS(SELECT 'True' FROM sys.databases WHERE name LIKE 'ExamDatabase') DROP DATABASE  ExamDatabase
	  CREATE DATABASE ExamDatabase
	use [ExamDatabase]
	
		CREATE TABLE [dbo].[REGIONS](
	[REGION_ID] [nchar] (20) NOT NULL,
	[REGION_NAME] [nvarchar](50) NOT NULL)
	ALTER TABLE [DBO].[REGIONS] ADD CONSTRAINT PK_REGION PRIMARY KEY(REGION_ID);

		CREATE TABLE [dbo].[COUNTRIES](
	[COUNTRY_ID] [nchar] (20) NOT NULL,
	[COUNTRY_NAME] [nvarchar](50) NOT NULL,
	[REGIOM_ID] [nchar] (20)  NOT NULL)
	ALTER TABLE [DBO].[COUNTRIES] ADD CONSTRAINT PK_COUNTRIES PRIMARY KEY(COUNTRY_ID);

	    CREATE TABLE [dbo].[LOCATIONS](
	[LOCATION_ID] [nchar] (20)  NOT NULL,
	[STREET_ADRESS] [nvarchar](50) NOT NULL,
	[POSTAL_CODE] [nvarchar](50) NOT NULL,
	[CITY] [nvarchar](50) NOT NULL,
	[STATE_PROVINCE] [nvarchar](50) NOT NULL,
	[COUNTRY_ID] [nchar] (20)  NOT NULL)
	ALTER TABLE [DBO].[LOCATIONS] ADD CONSTRAINT PK_LOCATIONS PRIMARY KEY(LOCATION_ID);

	    CREATE TABLE [dbo].[DEPARTMENTS](
	[DEPARTMENT_ID] [nchar] (20)  NOT NULL,
	[DEPARTMENT_NAME] [nvarchar](50) NOT NULL,
	[MANAGER_ID] [nchar] (20) NULL,
	[LOCATION_ID] [nchar] (20)  NOT NULL)
	ALTER TABLE [DBO].[DEPARTMENTS] ADD CONSTRAINT PK_DEPARTMENTS PRIMARY KEY(DEPARTMENT_ID);

		CREATE TABLE [dbo].[EMPLOYEES](
	[EMPLOYEE_ID] [nchar] (20)  NOT NULL,
	[FIRST_NAME] [nvarchar](50) NOT NULL,
	[LAST_NAME] [nvarchar](50) NOT NULL,
	[EMAIL] [nvarchar](50) NOT NULL,
	[PHONE_NUMBER] [nvarchar](50) NOT NULL,
	[HIRE_DATE] [date] NULL,--���� ������ �� ������
	[JOB_ID] [nchar] (20)  NOT NULL,
	[SALARY] [money] NOT NULL,--��������
	[COMMISSION_PCT] [float] NULL,
	[MANAGER_ID] [nchar] (20) NULL,
	[DEPARTMENT_ID] [nchar] (20)  NOT NULL)
	ALTER TABLE [DBO].[EMPLOYEES] ADD CONSTRAINT PK_EMPLOYEES PRIMARY KEY(EMPLOYEE_ID);

		CREATE TABLE [dbo].[JOBS](
	[JOB_ID] [nchar] (20)  NOT NULL,
	[JOB_TITLE] [nvarchar](50) NOT NULL,--���������
	[MIN_SALARY] [money] NOT NULL,
	[MAX_SALARY] [money] NOT NULL)
	ALTER TABLE [DBO].[JOBS] ADD CONSTRAINT PK_JOBS PRIMARY KEY(JOB_ID);

		CREATE TABLE [dbo].[JOB_HISTORY](
	[EMPLOYEE_ID] [nchar] (20)  NOT NULL,
	[START_DATE] [date] NOT NULL,
	[END_DATE] [date] NOT NULL,
	[JOB_ID] [nchar] (20)  NOT NULL,
	[DEPARTMENT_ID] [nchar] (20)  NOT NULL)
	------------------------------------1-----------------
	select 
	departments.DEPARTMENT_NAME,
	employee.SALARY
	from
	[dbo].[DEPARTMENTS] as departments
	left join 
	[dbo].[EMPLOYEES] as employee
	on departments.DEPARTMENT_ID=employee.DEPARTMENT_ID
	where employee.SALARY>12000
	order by employee.SALARY desc;
	-----------------------------------2------------------
		select 
	departments.DEPARTMENT_NAME,
	employee.LAST_NAME,
	employee.FIRST_NAME,
	jobs.JOB_TITLE,
	employee.SALARY
	from
	[dbo].[DEPARTMENTS] as departments
	left join 
	[dbo].[EMPLOYEES] as employee
	on departments.DEPARTMENT_ID=employee.DEPARTMENT_ID
	left join 
	[dbo].[JOBS] as jobs
	on jobs.JOB_ID=employee.JOB_ID
	where employee.SALARY>12000
	order by employee.SALARY desc;
	--------------------------3-----------------------------
	--�������� ������ ����������� � ��������� �� �����, ���������, ���������� ����� � � ��������� ������, � ������� �� �������� ������
	 select
	 employee.FIRST_NAME,
	 employee.LAST_NAME,
	 jobs.JOB_TITLE,
	 employee.SALARY,
	 departments.DEPARTMENT_NAME,
	 max(job_history.START_DATE)
	 from
	 [dbo].[DEPARTMENTS] as departments
	left join 
	[dbo].[EMPLOYEES] as employee
	on departments.DEPARTMENT_ID=employee.DEPARTMENT_ID
	left join 
	[dbo].[JOBS] as jobs
	on jobs.JOB_ID=employee.JOB_ID
	left join
	[dbo].[JOB_HISTORY] as job_history
	on job_history.EMPLOYEE_ID=employee.EMPLOYEE_ID
	group by  employee.FIRST_NAME,
	 employee.LAST_NAME,
	 jobs.JOB_TITLE,
	 employee.SALARY,
	 departments.DEPARTMENT_NAME,
	 job_history.START_DATE
	 --�������� ������ �����������, ��� ���������� ������ ������������� �� 9 
	 select
	 employee.FIRST_NAME,
	 employee.LAST_NAME,employee.PHONE_NUMBER
	 from
	 [dbo].[EMPLOYEES] as employee
	 where employee.PHONE_NUMBER LIKE '%9';
	 --5. �������� ������ �����������, ��� ���������� ������ ���������� � 590 � ������������� �� 0 
	  select
	 employee.FIRST_NAME,
	 employee.LAST_NAME,employee.PHONE_NUMBER
	 from
	 [dbo].[EMPLOYEES] as employee
	 where employee.PHONE_NUMBER LIKE '590%0';
	 --6. �������� ������ ���������� � �������� �������, �������� ��� ��������� 
	 select 
	 employee.FIRST_NAME,
	 employee.LAST_NAME,
	 jobs.JOB_TITLE,
	 departments.DEPARTMENT_NAME
	 from
	  [dbo].[DEPARTMENTS] as departments
	left join 
	[dbo].[EMPLOYEES] as employee
	on departments.DEPARTMENT_ID=employee.DEPARTMENT_ID
	left join 
	[dbo].[JOBS] as jobs
	on jobs.JOB_ID=employee.JOB_ID 
	where jobs.JOB_TITLE LIKE '% Manager';
	 --�������� ������ ����������� � ��������� �� �����, ���������, ���������� ����� � � ��������� ������,
	 --� ������� �� �������� ������ � ����� ����������������� ���������� 
	-- 	 select 
	-- employee.FIRST_NAME,
	-- employee.LAST_NAME,
	-- jobs.JOB_TITLE,
	-- employee.SALARY,
	-- departments.DEPARTMENT_NAME
	-- from
	--  [dbo].[DEPARTMENTS] as departments
	--left join 
	--[dbo].[EMPLOYEES] as employee
	--on departments.DEPARTMENT_ID=employee.DEPARTMENT_ID
	--left join 
	--[dbo].[JOBS] as jobs
	--on jobs.JOB_ID=employee.JOB_ID
	 --��������� ������� ���������� ����� ��������� 
	 select
	 AVG(employee.SALARY)
	 from
	 [dbo].[EMPLOYEES] as employee
	 left join 
	 [dbo].[JOBS] as jobs
	 on jobs.JOB_ID=employee.JOB_ID
	 where jobs.JOB_TITLE LIKE '% Manager';
	 --9. �������� ������ ������� � ���������� ����������� ������ �� ������� 
	 select 
	 departments.DEPARTMENT_NAME,
	 count(	 departments.DEPARTMENT_NAME) as number_employees
	 from
	  [dbo].[EMPLOYEES] as employee
	 left join 
	 [dbo].[JOBS] as jobs
	 on jobs.JOB_ID=employee.JOB_ID
	 left join
	 [dbo].[DEPARTMENTS] as departments
	 on departments.DEPARTMENT_ID=employee.DEPARTMENT_ID
	 group by departments.DEPARTMENT_NAME;
	 --10. �������� ������ ������� � ������� ���������� ����� � ���
	  select 
	 departments.DEPARTMENT_NAME,
	 AVG(employee.SALARY) as AVG_Salary
	 from
	  [dbo].[EMPLOYEES] as employee
	 left join
	 [dbo].[DEPARTMENTS] as departments
	 on departments.DEPARTMENT_ID=employee.DEPARTMENT_ID
	 group by departments.DEPARTMENT_NAME;
	 --11. �������� ������ ������� � ��������� ������ ������� �� ������ ����� ��� ����������� 
	 	  select 
	 departments.DEPARTMENT_NAME,
	 SUM(employee.SALARY)
	 from
	  [dbo].[EMPLOYEES] as employee
	 left join
	 [dbo].[DEPARTMENTS] as departments
	 on departments.DEPARTMENT_ID=employee.DEPARTMENT_ID
	 group by departments.DEPARTMENT_NAME;
	 --z12. ���������� ���� �� ������, � ������� �������� ����������, ��������������� ������������ ������ 