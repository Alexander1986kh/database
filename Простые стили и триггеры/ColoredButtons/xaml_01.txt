    <Window.Background>
        <LinearGradientBrush>
            <GradientStop Color="Red" Offset="0" />
            <GradientStop Color="Yellow" Offset=".5" />
            <GradientStop Color="Lime" Offset="1" />
        </LinearGradientBrush>
    </Window.Background>