use AdventureWorks2008
IF EXISTS (SELECT * FROM SYS.triggers WHERE [NAME] ='my_safety_create_log_trigger')
--IF (OBJECT_ID (N'my_safety_create_log_trigger',N'TR') is not null)
drop trigger  my_safety_create_log_trigger ON DATABASE;
	GO
 CREATE TRIGGER my_safety_create_log_trigger
 ON DATABASE 
 FOR CREATE_TABLE 
 AS
 INSERT INTO [dbo].[MYLOGS] ([ACTION], [TBL_NAME], [ID_ROW], [LOG_INFO], [LOG_TIME]) VALUES 
 ('CREATE', 'NOTDEF',-1, N'выполнена операция CREATE_TABLE',getdate());

 go
 IF EXISTS (SELECT * FROM SYS.triggers WHERE [NAME] ='my_safety_drop_log_trigger')
--IF (OBJECT_ID (N'my_safety_create_log_trigger',N'TR') is not null)
drop trigger  my_safety_drop_log_trigger ON DATABASE;
	GO
 CREATE TRIGGER my_safety_drop_log_trigger
 ON DATABASE 
 FOR DROP_TABLE 
 AS
 INSERT INTO [dbo].[MYLOGS] ([ACTION], [TBL_NAME], [ID_ROW], [LOG_INFO], [LOG_TIME]) VALUES 
 ('DROP', 'NOTDEF',-2, N'выполнена операция DROP_TABLE',getdate());

  go
 IF EXISTS (SELECT * FROM SYS.triggers WHERE [NAME] ='my_safety_alter_log_trigger')
--IF (OBJECT_ID (N'my_safety_create_log_trigger',N'TR') is not null)
drop trigger  my_safety_alter_log_trigger ON DATABASE;
	GO
 CREATE TRIGGER my_safety_alter_log_trigger
 ON DATABASE 
 FOR ALTER_TABLE 
 AS
 INSERT INTO [dbo].[MYLOGS] ([ACTION], [TBL_NAME], [ID_ROW], [LOG_INFO], [LOG_TIME]) VALUES 
 ('ALTER', 'NOTDEF',-2, N'выполнена операция ALTER_TABLE',getdate());

 Select *
 from 
 MYLOGS