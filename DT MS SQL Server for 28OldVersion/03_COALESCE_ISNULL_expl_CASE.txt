//���� ������

SELECT   ProductNumber, ProductLine ,
   Name
FROM Production.Product
ORDER BY ProductNumber;

//������ CASE ��� ������ �������� NULL
USE AdventureWorks2008;
GO
SELECT   ProductNumber, Category =
      CASE ProductLine
         WHEN 'R' THEN 'Road'
         WHEN 'M' THEN 'Mountain'
         WHEN 'T' THEN 'Touring'
         WHEN 'S' THEN 'Other sale items'
         ELSE 'Not for sale'
      END,
   Name
FROM Production.Product
ORDER BY ProductNumber;
GO

//������ ������ ������ NULLIF ����� case
SELECT 
ProductID, 
MakeFlag, 
FinishedGoodsFlag,
'Null if Equal' =
   CASE
       WHEN MakeFlag = FinishedGoodsFlag THEN NULL
       ELSE MakeFlag
   END
FROM Production.Product
WHERE ProductID < 10;

//������ ������������� NULLIF ��� ����������� �������
SELECT ProductID, MakeFlag, FinishedGoodsFlag, 
   NULLIF(MakeFlag,FinishedGoodsFlag)AS 'Null if Equal'
FROM Production.Product
WHERE ProductID < 10;
