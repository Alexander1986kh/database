﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventDelegateTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Water cold = new Water("cold");
            cold.isRunning += WaterConditionChanged;
            cold.isStopped += WaterConditionChanged;
            cold.run();
            cold.stop();
        }

        private static void WaterConditionChanged(string message)
        {
            Console.WriteLine(message);
        }
    }
}