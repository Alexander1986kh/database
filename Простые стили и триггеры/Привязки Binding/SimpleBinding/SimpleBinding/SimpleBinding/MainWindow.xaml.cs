﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SimpleBinding
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            testTextBlockFilling();
            testListBoxFilling();
        }
        private void testListBoxFilling()
        {
            List<TodoItem> items = new List<TodoItem>();
            items.Add(new TodoItem() { Title = "Complete this WPF tutorial", Completion = 45 });
            items.Add(new TodoItem() { Title = "Learn C#", Completion = 80 });
            items.Add(new TodoItem() { Title = "Wash the car", Completion = 0 });
            lbTodoList.ItemsSource = items;
            
        }
        private void testTextBlockFilling()
        {
            List<TextViewer> list = new List<TextViewer>();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 100; i++)
            {
                list.Add(new TextViewer(String.Format("{0},{1}", i.ToString(), "Some text...")));
                sb.AppendLine(list[i].DisplayText);
            }
            TextViewer tv = new TextViewer(sb.ToString());
            this.txtMyTextBox.DataContext = tv;
        }
    }
    public class TextViewer
    {
        private String datasource;
        public TextViewer(String source)
        {
            this.datasource = source;
        }

        public string DisplayText
        {
            get
            {
                if (datasource.Contains("<bold>") == false)
                {
                    //return "<bold>" + datasource + "</bold>";
                    return datasource;
                }
                return datasource;
            }
            set { datasource = value; }
        }
    }

}
