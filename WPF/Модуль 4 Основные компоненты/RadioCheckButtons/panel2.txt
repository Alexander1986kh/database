 <StackPanel Margin="10" x:Name="panel2">
                <Label FontWeight="Bold">Are you ready?</Label>
                <RadioButton>
                    <WrapPanel>
                        <Image Source="pics/bullet_blue.png"  Width="16" Height="16" Margin="0,0,5,0" />
                        <TextBlock Text="Yes" Foreground="Green" />
                    </WrapPanel>
                </RadioButton>
                <RadioButton Margin="0,5">
                    <WrapPanel>
                        <Image Source="pics/bullet_red.png" Width="16" Height="16" Margin="0,0,5,0" />
                        <TextBlock Text="No" Foreground="Red" />
                    </WrapPanel>
                </RadioButton>
                <RadioButton IsChecked="True">
                    <WrapPanel>
                        <Image Source="pics/bullet_green.png" Width="16" Height="16" Margin="0,0,5,0" />
                        <TextBlock Text="Maybe" Foreground="Gray" />
                    </WrapPanel>
                </RadioButton>
            </StackPanel>
