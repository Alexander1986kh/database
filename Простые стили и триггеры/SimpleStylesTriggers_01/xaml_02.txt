 <Window.Resources>
        <Style x:Key="RectStyle1" TargetType="Rectangle">
            <Style.Triggers>
                <Trigger Property="IsMouseOver" Value="true">
                    <Setter Property="Cursor" Value="Hand"/>
                </Trigger>
            </Style.Triggers>
        </Style>
        <Style TargetType="Button">
            <Style.Triggers>
                <Trigger Property="IsMouseOver" Value="true">
                    <Setter Property="Foreground" Value="Blue" />
                    <Setter Property="FontSize" Value="16"/>
                    <Setter Property="Cursor" Value="Hand"/>
                </Trigger>
                <Trigger Property="IsPressed"  Value="true">
                    <Setter Property="Foreground" Value="WhiteSmoke" />
                </Trigger>
            </Style.Triggers>
        </Style>
    </Window.Resources>
    <Grid>
        <StackPanel>
            <Rectangle x:Name="myrect1" 
                       Width="100" Height="50" 
                       Fill="Red"></Rectangle>
            <Rectangle Style="{StaticResource RectStyle1}" 
                       x:Name="myrect2" 
                       Width="100" Height="50" 
                       Fill="Yellow"></Rectangle>
            <Rectangle x:Name="myrect3" 
                       Width="100" Height="50" 
                       Fill="Green"></Rectangle>
            <Button x:Name="btn1" Width="150" Height="25">Press me</Button>
            <Button x:Name="btn2" Width="150" Height="25">Push me</Button>
            <Button x:Name="btn3" Width="150" Height="25">Click me</Button>

        </StackPanel>

    </Grid>