<StackPanel Margin="10" x:Name="panel2">
                <ComboBox>
                    <ComboBoxItem>
                        <StackPanel Orientation="Horizontal">
                            <Image Source="pics/bullet_green.png" />
                            <TextBlock Foreground="Red">Red</TextBlock>
                        </StackPanel>
                    </ComboBoxItem>
                    <ComboBoxItem>
                        <StackPanel Orientation="Horizontal">
                            <Image Source="pics/bullet_blue.png" />
                            <TextBlock Foreground="Green">Green</TextBlock>
                        </StackPanel>
                    </ComboBoxItem>
                    <ComboBoxItem>
                        <StackPanel Orientation="Horizontal">
                            <Image Source="pics/bullet_red.png" />
                            <TextBlock Foreground="Blue">Blue</TextBlock>
                        </StackPanel>
                    </ComboBoxItem>
                </ComboBox>
            </StackPanel>