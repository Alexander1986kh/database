use [AdventureWorks2008]
IF EXISTS (SELECT * FROM sys.tables where [name] = 'USERS_TO_ROLES')
DROP TABLE USERS_TO_ROLES;--����� ������������
create table USERS_TO_ROLES(
ID int identity(1,1) not null,
[USER_ID] int not null,
ROLE_ID int not null
);

IF EXISTS (SELECT * FROM sys.tables where [name] = 'USERS')
DROP TABLE USERS;
create table USERS(
ID int identity(1,1) not null,
[USER_NAME] varchar(20) not null,
);

IF EXISTS (SELECT * FROM sys.tables where [name] = 'ROLES')
DROP TABLE ROLES;
create table ROLES(
ID int identity(1,1) not null,
ROLE_NAME varchar(20) not null,--�������� ����, � ����������� � ������� ������������
[RULE] char(20) not null,--(������ U � ������, ������� �������� � ������� RULE � �������� ����� ��������� ������,
-- ������ I � ����� ��������� ������, ������ D � ����� �������) 
);



IF EXISTS (SELECT * FROM sys.tables where [name] = 'MYLOGS')
DROP TABLE MYLOGS;

create table MYLOGS(
MYLOGS_ID int identity(1,1) not null,
[ACTION] nchar(20) not null,--�������� ��� �������� ��� �����
TBL_NAME varchar(20) not null,--���� �������� ��� ��������- �� ��������
ID_ROW INT not null,--ID ������ �������, ���� ��������� � ���
LOG_INFO nvarchar(100) not null,--���������� ��������
LOG_TIME date not null--����� �������
);

--INSERT INTO [dbo].[MYLOGS] ([ACTION], [TBL_NAME], [ID_ROW], [LOG_INFO], [LOG_TIME])

ALTER TABLE [USERS_TO_ROLES] ADD CONSTRAINT PK_USERS_TO_ROLES_ID PRIMARY KEY (ID);
ALTER TABLE [USERS] ADD CONSTRAINT PK_USERS_ID PRIMARY KEY (ID);
ALTER TABLE [ROLES] ADD CONSTRAINT PK_ROLES_ID PRIMARY KEY (ID);
ALTER TABLE [MYLOGS] ADD CONSTRAINT PK_MYLOGS_MYLOGS_ID PRIMARY KEY (MYLOGS_ID);
ALTER TABLE [USERS_TO_ROLES] ADD CONSTRAINT FK_USERS_TO_ROLES_USER_ID FOREIGN KEY ([USER_ID]) REFERENCES USERS(ID);
ALTER TABLE [USERS_TO_ROLES] ADD CONSTRAINT FK_USERS_TO_ROLES_ROLE_ID FOREIGN KEY (ROLE_ID) REFERENCES ROLES(ID);
