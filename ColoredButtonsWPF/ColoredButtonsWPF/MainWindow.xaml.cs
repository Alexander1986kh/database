﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;

namespace ColoredButtonsWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void button1_Click(object sender, RoutedEventArgs e)
        {
            System.IO.StreamWriter sw = new System.IO.StreamWriter("D:\\MyData\\username.txt");
            sw.WriteLine(textBox1.Text);
            sw.Close();
        }
        private void button2_Click(object sender, RoutedEventArgs e)
        {
            System.IO.StreamReader sr = new System.IO.StreamReader("D:\\MyData\\username.txt");
            label1.Content = "Hello " + sr.ReadToEnd();
            sr.Close();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            DoubleAnimation myDoubleAnimation = new DoubleAnimation();
            myDoubleAnimation.From = 1.0;
            myDoubleAnimation.To = 0.0;
            myDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(5));
            
            Button1.Opacity=new Double(2);

            //var da = new DoubleAnimation(-360, 360, new Duration(TimeSpan.FromSeconds(2)));
            //var rt = new RotateTransform();
            //rect2.RenderTransform = rt;
            //rect2.RenderTransformOrigin = new Point(0.5, 0.5);
            //da.RepeatBehavior = RepeatBehavior.Forever;
            //rt.BeginAnimation(RotateTransform.AngleProperty, da);
        }

    }
}
