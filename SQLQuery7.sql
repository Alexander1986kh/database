CREATE PROCEDURE CALC_SOLUTION_X1X2
(
@a INT,--INPUT
@b INT,--INPUT
@c INT,--INPUT
@OUT_RESULT VARCHAR(200) OUT,
@x1 FLOAT OUT,
@x2 FLOAT OUT
)
AS
BEGIN 
DECLARE @D FLOAT;
SELECT 
	 @D=@b*@b-4*@a*@c;

IF(@D<0)
	BEGIN
		SET @x1=0.0;
		SET @x2=0.0;
		SET @OUT_RESULT='NO';
	END
ELSE 
	BEGIN
		SELECT @x1=(-@B+SQRT(@D))/2*@a;
		SELECT @x2=(-@B-SQRT(@D))/2*@a;
		SET @OUT_RESULT='YES';
	END
END

Declare @res varchar(200)   -- Declaring the variable to collect the discriminant
Declare @x1 float    -- Declaring the variable to collect the x1
Declare @x2 float    -- Declaring the variable to collect the x2
set @res = 'NOT def';
Execute [dbo].CALC_SOLUTION_X1X2 1 ,0,-9, @res output, @x1 output, @x2 output
select @res as d,@x1 as x1,@x2 as x2 -- "Select" Statement is used to show the output from Procedure
