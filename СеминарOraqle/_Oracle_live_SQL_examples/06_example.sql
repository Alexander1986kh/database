select 


j.JOB_TITLE , j.MIN_SALARY, j.MAX_SALARY
, 
jh.START_DATE, jh.END_DATE 
, dep.DEPARTMENT_NAME
, empl.FIRST_NAME, empl.LAST_NAME


from HR.JOB_HISTORY jh
left join HR.JOBS j on j.JOB_ID=jh.JOB_ID
left join HR.EMPLOYEES empl on j.JOB_ID=empl.JOB_ID
left join HR.DEPARTMENTS dep on empl.DEPARTMENT_ID=dep.DEPARTMENT_ID