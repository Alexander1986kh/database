USE [SampleDb]
GO

INSERT INTO [dbo].[Project]
           ([Number]
           ,[ProjectName]
           ,[Budget])
     VALUES
           ('p1'  ,	'Apollo'    ,    	120000),
		   ('p2'  ,	'Gemini'    ,    	95000),
		   ('p3'  ,	'Mercury'    ,    	186500)
GO