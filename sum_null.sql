declare @s1 int;
declare @s2 int;
declare @s3 int;
declare @s4 int;
declare @s5 int;

set @s1=100;
set @s2=300;
set @s3=null;
set @s4=200;
set @s5=100;

select (@s1+@s2+@s4+@s5) as summ;--возвращает 700
select (@s1+@s2+@s3+@s4+@s5) as sum_null;--возвращает null

select('Salary '+cast(@s1 as varchar(100)));-- Salary 100

select ('Salary '+cast(@s3 as varchar(90)));-- null

