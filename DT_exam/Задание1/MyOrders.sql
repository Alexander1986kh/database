use ExamDatabase1
	insert into [dbo].[MYORDERS] (DESCRIPTION, ORDERS_DATE,TOTAL_COSTS, MYCLIENTS_ID) values ('Bread,Salt', '26.08.2016', 25.00, 1) ;
	insert into [dbo].[MYORDERS] (DESCRIPTION, ORDERS_DATE,TOTAL_COSTS, MYCLIENTS_ID) values ('Bread, water', '26.08.2016', 15.00, 2) ;
	insert into [dbo].[MYORDERS] (DESCRIPTION, ORDERS_DATE,TOTAL_COSTS, MYCLIENTS_ID) values ('Salt', '26.08.2016', 9.00, 3) ;
	select
	*
	from
	[dbo].[MYORDERS]
	--��� ������� ������ �������� ������, ������� ������� �������� � �������, ���� ������,
	-- ����������� ����� ������ � � ����� ������, �� ��������� ��������� ������� �� �������. 
	select 
	clients.NAME,
	clients.PHONE,
	orders.ORDERS_DATE,
	orders.TOTAL_COSTS,
	SUM(order_pos.COUNT*order_pos.PRICE) as Sum_Order
	from 
	[dbo].[MYORDERS] as orders
	left join
	[dbo].[MYCLIENTS]as clients
	on orders.MYCLIENTS_ID=clients.MYCLIENT_ID
	left join
	[dbo].[MYORDER_POSITIONS] as order_pos
	on order_pos.MYORDER_ID=orders.MYORDER_ID
	group by clients.NAME, clients.PHONE,
	orders.ORDERS_DATE, orders.TOTAL_COSTS
